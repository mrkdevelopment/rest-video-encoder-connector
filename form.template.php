<?php
/**
 * @package RVE_connector
 * @version 0.1
 *
 * // TODO : add validators on fields
 */
 defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
?>

<form action="options-general.php?page=rve-settings" method="post">
 <h2>Rest Video Encoder URL</h2>
 <input type="text" name="rve-url" value="<?php echo $url; ?>" />

 <h2>RVE Encoder Token</h2>
 <input type="text" name="rve-api-token" value="<?php echo $token; ?>"/>

 <h2>Application ID</h2>
 <input type="text" name="rve-app-id" value="<?php echo $appId; ?>"/>
 
 <button>Submit</button>
</form>