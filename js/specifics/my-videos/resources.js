app.factory('Videos', ['$resource', function($resource){
	return $resource(
		RVE_URL + '/rve/api/1.0/videos/:id', 
		{id : '@id'}, 
		{ 
			'get':    {method:'GET', params: {include:'file'}}
		}
	);
}]).factory('Status', ['$resource', function($resource){
    return $resource(
        RVE_URL + '/rve/api/1.0/status/', 
        {}, 
        { 
            'get':    {method:'GET', params: {}}
        }
    );
}]).factory('Session', function(){
    var session = [];

    return {
        get : function(key){
            return session[key];
        },
        put: function(key, data){
            session[key] = data
        }
    }
});