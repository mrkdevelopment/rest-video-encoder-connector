var app = angular.module('videosApp', ['ngRoute', 'ngResource']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/encoding', {
      templateUrl: TEMPLATE_URL + 'encoding.html',
      controller: 'EncodingCtrl'
    }).when('/view/:id', {
    templateUrl: TEMPLATE_URL + 'view.html',
    controller: 'ViewVideoCtrl'
  }).when('/list', {
    templateUrl: TEMPLATE_URL + 'list.html',
    controller: 'ListVideosCtrl'
  })
  .when('/', {
    templateUrl: TEMPLATE_URL + 'status.html',
    controller: 'StatusCtrl'
  })
  .otherwise({
	 redirectTo: '/'
   });
}]);