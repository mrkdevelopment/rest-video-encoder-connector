app.controller('ViewVideoCtrl', function ViewCtrl($scope, Session, $location, $window, $routeParams){

	 $scope.init = function() {
	 	$scope.video = Session.get('video');
	 	if ($scope.video) {

	 		$scope.links = $scope.video.file.data.links;
	 		var playlist = [$scope.links.low]
	 		setTimeout(function() {
	 			jQuery('#flowplayer').flowplayer({
	 				playlist: playlist
	 			});
	 		}, 100);
	 	} else {
	 		$location.path('/');
	 	}
	 }();
	
	

}).controller('ListVideosCtrl', function ViewCtrl($scope, Videos, Session, $location, $window, $routeParams){
	 $scope.init = function() {
	 	Videos.get({}, function(response) {
	 		$scope.videos = response.data;
	 	});
	 }();
}).controller('EncodingCtrl', function ViewCtrl($scope, Session, $location, $window, $routeParams){

}).controller('StatusCtrl', function StatusCtrl($scope, Status, Session, $location, $window, $routeParams){
	/**
	 * Checks the status of the distant server
	 */
	var init = function() {
		Status.get({}, function(response) {
			$scope.status = 'ok';
			Session.put('status', 'ok');
			Session.put('rve_url', RVE_URL);
			$location.path('/list');
		}, function(reponse) {
			$scope.status = 'ko';
		});
	}();
}).controller('ViewVideoCtrl', function ViewCtrl($scope, Videos, Session, $location, $window, $routeParams){

	 $scope.init = function() {
	 	var videoId = $routeParams.id;
	 	Videos.get({id:videoId}, function(response) {
	 		$scope.video = response.data
	 		$scope.links = $scope.video.file.data.links;
	 		var playlist = [$scope.links.low]
	 		setTimeout(function() {
	 			jQuery('#flowplayer').flowplayer({
	 				playlist: playlist
	 			});
	 		}, 100);
	 	}, function() {
	 		$location.path('/');
	 	});
	 }();
	
	

});