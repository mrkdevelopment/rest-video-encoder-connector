var app = angular.module('videosApp', ['ngRoute', 'ngResource']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/encoding', {
      templateUrl: TEMPLATE_URL + 'encoding.html',
      controller: 'EncodingCtrl'
    }).when('/view', {
    templateUrl: TEMPLATE_URL + 'view.html',
    controller: 'ViewVideoCtrl'
  })
  .when('/', {
    templateUrl: TEMPLATE_URL + 'status.html',
    controller: 'StatusCtrl'
  })
  .otherwise({
	 redirectTo: '/'
   });
}]);