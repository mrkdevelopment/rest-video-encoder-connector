app.controller('ViewVideoCtrl', function ViewCtrl($scope, Session, $location, $window, $routeParams){

	 $scope.init = function() {
	 	$scope.video = Session.get('video');
	 	if ($scope.video) {

	 		$scope.links = $scope.video.file.data.links;
	 		var playlist = [$scope.links.low]
	 		setTimeout(function() {
	 			jQuery('#flowplayer').flowplayer({
	 				playlist: playlist
	 			});
	 		}, 100);
	 	} else {
	 		$location.path('/');
	 	}
	 }();
	
	

}).controller('EncodingCtrl', function ViewCtrl($scope, Session, $location, $window, $routeParams){

}).controller('StatusCtrl', function StatusCtrl($scope, Videos, Session, $location, $window, $routeParams){
	/**
	 * Checks the status of the distant server
	 */
	var init = function() {

		Videos.get({id:VIDEO_ID}, function(response) {

			$scope.status = response.data.file.data.status;
			Session.put('video', response.data);

			if ($scope.status == 'ready') {
				$location.path('/view');	
			} else {
				$location.path('/encoding');
			}
			
		}, function(reponse) {
			$scope.status = 'ko';
		});
	}();

});