app.controller('VideoListCtrl', function VideoListCtrl($scope, Videos, $window, $routeParams){
	var init = function() {
		
		Videos.get({}, function(response) {
			$scope.videos = response.data;
		});
	}();
}).controller('VideoCtrl', function VideoCtrl($scope, Videos, $location, Session, $window, $routeParams){
	
	/**
	 * Initialization, checking if the status has been tested
	 */
	$scope.init = function() {
		if (!Session.get('status')) {
			$location.path('/');
		}

		Session.put('api_key', API_KEY);
		Session.put('app_id', APP_ID);
	}();

	/**
	 * Create video will create a page ready to receive the file then move to the video uploader
	 */
	$scope.createVideo = function() {
		Videos.post($scope.video, function(response) {
			var id = response.id;
			Session.put('video-id', id);
			$location.path('/upload');
		});
	}

}).controller('UploadCtrl', function UploadCtrl($scope, Videos, Session, $location, $window, $routeParams){
	
	$scope.fileAdded = function(file, event) {
		var max_size = Session.get('max-size');
		if( max_size && (max_size < file.size)){
			var limit = parseFloat(max_size/1048576).toFixed(2);
			var size = parseFloat(file.size/1048576).toFixed(2);
			$scope.info = file.name + ', size ' + size + "M exeeds the limit of " + limit + 'M';

			return false;
		}
	}

	var init = function() {
		if (!Session.get('video-id')) {
			$location.path('/add');
		} 

		$scope.rve_url = Session.get('rve_url');
		$scope.api_key = Session.get('api_key');
		$scope.app_id = Session.get('app_id');
		$scope.video_id = Session.get('video-id');
		

		setTimeout(function() {
			jQuery('.accept-videos').children('input').attr('accept', 'video/mp4,video/mpeg,video/quicktime');
		}, 100);
	}();

	
	$scope.fileUploaded = function(message, file) {
		var createdFile = {};
		createdFile.id = message.id;
		createdFile.path = message.path;

	}

}).controller('StatusCtrl', function StatusCtrl($scope, Status, Session, $location, $window, $routeParams){
	/**
	 * Checks the status of the distant server
	 */
	var init = function() {
		Status.get({}, function(response) {
			$scope.status = 'ok';
			Session.put('status', 'ok');
			Session.put('rve_url', RVE_URL);
			Session.put('max-size', response.max_size);
			$location.path('/add');
		}, function(reponse) {
			$scope.status = 'ko';
		});
	}();

});