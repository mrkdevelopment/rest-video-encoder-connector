var app = angular.module('videosApp', ['ngRoute', 'flow', 'ngResource']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/upload', {
      templateUrl: TEMPLATE_URL + 'upload.html',
      controller: 'UploadCtrl'
    }).when('/add', {
    templateUrl: TEMPLATE_URL + 'add.html',
    controller: 'VideoCtrl'
  })
  .when('/', {
    templateUrl: TEMPLATE_URL + 'status.html',
    controller: 'StatusCtrl'
  })
  .otherwise({
	 redirectTo: '/'
   });
}]);