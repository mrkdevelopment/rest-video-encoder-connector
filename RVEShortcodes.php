<?php
/**
 * @package RVE_connector
 * @version 0.1
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/**
 * RVE shortcodes
 * Defines shortcode used for page creation and video upload
 */
class RVEShortcodes {
	/**
	 * Create a video by communicating with the REST Video Encoder
	 */
	public function createVideo() {
		$user_id = get_current_user_id();
		$rveUrl = get_option('rve-url');
		$rveToken = get_option('rve-api-token');
		$rveAppId = get_option('rve-app-id', false);

		include 'views/createVideo.php';
		$this->enqueueScriptsVideoCreation();
		$this->enqueueStyleVideoCreation();
	}

	/**
	 * Enqueue scripts needed for the video creation page
	 */
	public function enqueueScriptsVideoCreation() {

		wp_enqueue_script('angular', plugins_url( 'js/ng-vendors/angular-1.3.14/angular.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-resource', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-resource.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-route', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-route.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('ng-flow', plugins_url( 'js/ng-vendors/ng-flow/ng-flow-standalone.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-app', plugins_url( 'js/specifics/videos/app.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-interceptor', plugins_url( 'js/specifics/http-interceptors.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-controller', plugins_url( 'js/specifics/videos/controller.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-resources', plugins_url( 'js/specifics/videos/resources.js', __FILE__ ), array('jquery'));

	}

	/**
	 * Register the video style
	 */
	public function enqueueStyleVideoCreation() {
		wp_enqueue_style('bootstrap', plugins_url( 'css/bootstrap.min.css', __FILE__ ));
	}


	/**
	 * Create a video by communicating with the REST Video Encoder
	 */
	public function viewVideo($args) {
		$user_id = get_current_user_id();
		$rveUrl = get_option('rve-url');
		$rveToken = get_option('rve-api-token');
		$rveAppId = get_option('rve-app-id');
		$videoId = $args['id'];

		include 'views/viewVideo.php';
		$this->enqueueScriptsVideoView();
		$this->enqueueStyleVideoView();
	}

	/**
	 * Enqueue scripts needed for the video creation page
	 */
	public function enqueueScriptsVideoView() {

		wp_enqueue_script('angular', plugins_url( 'js/ng-vendors/angular-1.3.14/angular.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-resource', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-resource.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-route', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-route.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-app', plugins_url( 'js/specifics/view-video/app.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-interceptor', plugins_url( 'js/specifics/http-interceptors.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-controller', plugins_url( 'js/specifics/view-video/controller.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-resources', plugins_url( 'js/specifics/view-video/resources.js', __FILE__ ), array('jquery'));

		wp_enqueue_script('flowplayer', plugins_url( 'js/vendors/flowplayer-5.5.2/flowplayer.min.js', __FILE__ ), array('jquery'), '5.5.2');

	}

	/**
	 * Register the video style
	 */
	public function enqueueStyleVideoView() {
		wp_enqueue_style('bootstrap', plugins_url( 'css/bootstrap.min.css', __FILE__ ));
		wp_enqueue_style('flowplayer', plugins_url( 'js/vendors/flowplayer-5.5.2/skin/minimalist.css', __FILE__ ));
	}


	/**
	 * Create a video by communicating with the REST Video Encoder
	 */
	public function myVideos($args) {
		$user_id = get_current_user_id();
		$rveUrl = get_option('rve-url');
		$rveToken = get_option('rve-api-token');
		$rveAppId = get_option('rve-app-id');
		include 'views/myVideos.php';
		$this->enqueueScriptsMyVideos();
		$this->enqueueStyleMyVideos();
	}

	/**
	 * Enqueue scripts needed for the video creation page
	 */
	public function enqueueScriptsMyVideos() {

		wp_enqueue_script('angular', plugins_url( 'js/ng-vendors/angular-1.3.14/angular.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-resource', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-resource.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('angular-route', plugins_url( 'js/ng-vendors/angular-1.3.14/angular-route.min.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-app', plugins_url( 'js/specifics/my-videos/app.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-interceptor', plugins_url( 'js/specifics/http-interceptors.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-controller', plugins_url( 'js/specifics/my-videos/controller.js', __FILE__ ), array('jquery'));
		wp_enqueue_script('rve-resources', plugins_url( 'js/specifics/my-videos/resources.js', __FILE__ ), array('jquery'));

		wp_enqueue_script('flowplayer', plugins_url( 'js/vendors/flowplayer-5.5.2/flowplayer.min.js', __FILE__ ), array('jquery'), '5.5.2');

	}

	/**
	 * Register the video style
	 */
	public function enqueueStyleMyVideos() {
		wp_enqueue_style('bootstrap', plugins_url( 'css/bootstrap.min.css', __FILE__ ));
		wp_enqueue_style('flowplayer', plugins_url( 'js/vendors/flowplayer-5.5.2/skin/minimalist.css', __FILE__ ));
	}

}