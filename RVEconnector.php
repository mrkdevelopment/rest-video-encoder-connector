<?php
/**
 * @package RVE_connector
 * @version 0.1
 */
/*
  Plugin Name: RVE_connector
  Plugin URI: http://mrkdevelopment.com/RVE_connector
  Description: Connector to the Rest Video Encoder 
  Author: Greg Duche 
  Version: 0.1
  Author URI: http://mrkdevelopment.com
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require('RVEShortcodes.php');

  // 1. Admin menu - settings 
  // 2. pages management
  	// 2.1 Create a new page
  	// 2.2 Upload a video to it
  // 3. video state
  // 4. view the video

/**
 * RVE connector
 * Connecting to the REST video encoder
 */
class RVEConnector {

	/**
	 * Registers the plugin end points.
	 */
	public function __construct() {
		add_action( 'admin_menu', array(&$this, 'admin_menu' ) );	
		$this->manageShortcodes();
	}


	/**
	 * Adding the admin menu in srettings
	 */
	public function admin_menu() {
		add_options_page('RVE Settings', 'RVE SEttings', 'manage_options', 'rve-settings', array(&$this, 'rveSettings'));
	}

	/**
	 * Defining the settings menu
	 */
	public function rveSettings() {
		$this->rveSettingsPostActions();

		$url = get_option('rve-url', null);
		$token = get_option('rve-api-token', null);
		$appId = get_option('rve-app-id', null);
		
		include 'form.template.php';
	}

	/**
	 * Managing post actions for settings
	 */
	public function rveSettingsPostActions() {
		if (isset($_POST) && !empty($_POST)) {

			$url = get_option('rve-url', false);
			$token = get_option('rve-api-token', false);
			$appId = get_option('rve-app-id', false);

			if (isset($_POST['rve-url'])) {
				$rveUrl = $_POST['rve-url'];
				if ($url) {
					update_option('rve-url', $rveUrl);
				} else {
					add_option('rve-url', $rveUrl);	
				}
			}

			if (isset($_POST['rve-api-token'])) {
				$rveToken = $_POST['rve-api-token'];
				if ($token) {
					update_option('rve-api-token', $rveToken);
				} else {
					add_option('rve-api-token', $rveToken);	
				}
			}

			if (isset($_POST['rve-app-id'])) {
				$rveAppId = $_POST['rve-app-id'];
				if ($appId) {
					update_option('rve-app-id', $rveAppId);
				} else {
					add_option('rve-app-id', $rveAppId);	
				}
			}
		}
	}

	public function manageShortcodes() {
		$shortcodes = new RVEShortcodes();
		add_shortcode('rve_create_video', array(&$shortcodes, 'createVideo')); 
		add_shortcode('rve_view_video', array(&$shortcodes, 'viewVideo')); 
		add_shortcode('rve_my_videos', array(&$shortcodes, 'myVideos')); 
	}
}

function init_rve() {
	new RVEConnector();
}

add_action( 'init', 'init_rve', 1);
?>
