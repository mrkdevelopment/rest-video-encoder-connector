<?php
/**
 * @package RVE_connector
 * @version 0.1
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
?>
<script>
	API_KEY = '<?php echo $rveToken; ?>';
	APP_ID = '<?php echo $rveAppId; ?>';
	TEMPLATE_URL = '<?php echo plugins_url("rest-video-encoder-connector"); ?>/js/specifics/videos/templates/';
	RVE_URL = '<?php echo $rveUrl; ?>';
	MAX_SIZE = '<?php echo 1024*1024*5; ?>'
</script>

<div class="video-container" ng-app="videosApp">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div ng-view></div>
				</div>
			</div>
		</div>
	</div>
</div>

